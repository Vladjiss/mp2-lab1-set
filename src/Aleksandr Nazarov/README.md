# Методы программирования 2: Множества на основе битовых полей

## Цели и задачи

Цель данной работы — разработка структуры данных для хранения множеств с использованием битовых полей, а также освоение таких инструментов разработки программного обеспечения, как система контроля версий Git и фрэймворк для разработки автоматических тестов Google Test.

Выполнение работы предполагает решение следующих задач: 

 * Реализация класса битового поля TBitField согласно заданному интерфейсу.
 * Реализация класса множества TSet согласно заданному интерфейсу.
 * Обеспечение работоспособности тестов и примера использования.
 * Реализация нескольких простых тестов на базе Google Test.
 * Публикация исходных кодов в личном репозитории на GitHub.

Работа выполнялась на основе проекта-шаблона, содержащего следующее:

 * Интерфейсы классов битового поля и множества (h-файлы)
 * Готовый набор тестов для каждого из указанных классов
 * Пример использования класса битового поля и множества для решения задачи
 * поиска простых чисел с помощью алгоритма "Решето Эратосфена"

## Заголовочный файл TBitField

```C++
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.h - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#ifndef __BITFIELD_H__
#define __BITFIELD_H__

#include <iostream>

using namespace std;

typedef unsigned int TELEM;

class TBitField
{
private:
	int  BitLen; // длина битового поля - макс. к-во битов
	TELEM *pMem; // память для представления битового поля
	int  MemLen; // к-во эл-тов Мем для представления бит.поля

	// методы реализации
	int   GetMemIndex(const int n) const; // индекс в pМем для бита n       (#О2)
	TELEM GetMemMask(const int n) const; // битовая маска для бита n       (#О3)
public:
	TBitField(int len);                //                                   (#О1)
	TBitField(const TBitField &bf);    //                                   (#П1)
	~TBitField();                      //                                    (#С)

	// доступ к битам
	int GetLength(void) const;      // получить длину (к-во битов)           (#О)
	void SetBit(const int n);       // установить бит                       (#О4)
	void ClrBit(const int n);       // очистить бит                         (#П2)
	int  GetBit(const int n) const; // получить значение бита               (#Л1)

	// битовые операции
	int operator==(const TBitField &bf) const; // сравнение                 (#О5)
	int operator!=(const TBitField &bf) const; // сравнение
	TBitField& operator=(const TBitField &bf); // присваивание              (#П3)
	TBitField  operator|(const TBitField &bf); // операция "или"            (#О6)
	TBitField  operator&(const TBitField &bf); // операция "и"              (#Л2)
	TBitField  operator~(void);                // отрицание                  (#С)

	friend istream &operator >> (istream &istr, TBitField &bf);       //      (#О7)
	friend ostream &operator<<(ostream &ostr, const TBitField &bf); //      (#П4)
  // Структура хранения битового поля
  //   бит.поле - набор битов с номерами от 0 до BitLen
  //   массив pМем рассматривается как последовательность MemLen элементов
  //   биты в эл-тах pМем нумеруются справа налево (от младших к старшим)
  // О8 Л2 П4 С2
};
#endif

```

## Реализация класса TBitField

```C++
#include "tbitfield.h"
#include <math.h>

TBitField::TBitField(int len)
{
		if (len < 0)
			throw "Lenght of field must be natural number";
		BitLen = len;
		MemLen = (len / (sizeof TELEM * 8) + (bool)(len % (sizeof TELEM * 8)));
		pMem = new TELEM[MemLen];
		if (pMem == nullptr)
			throw "not enough memory";
		for (int i = 0; i < MemLen; i++)
			pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
		BitLen = bf.GetLength();
		MemLen = (BitLen / (sizeof TELEM * 8) + (bool)(BitLen % (sizeof TELEM * 8)));
		pMem = new TELEM[MemLen];
		if (pMem == nullptr)
			throw "not enough memory";
		for (int i = 0; i < MemLen; i++)
			pMem[i] = 0;
		for (int i = 0; i < bf.GetLength(); i++) 
			if (bf.GetBit(i))
				SetBit(i);
			else
				ClrBit(i);
}

TBitField::~TBitField()
{
		delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
		if (n < 0 || n >= BitLen)
			throw "wrong n";
		return  n / (sizeof TELEM * 8);
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
		if (n >= BitLen || n < 0)
			throw "wrong n";
		return 1 << (n - GetMemIndex(n) * sizeof TELEM * 8);
}

// доступ к битам битового поля

int TBitField::GetLength() const // получить длину (к-во битов)
{
	 return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
		if (n >= BitLen || n < 0)
			throw "wrong n";
		pMem[GetMemIndex(n)] |= GetMemMask(n);
}

void TBitField::ClrBit(const int n) // очистить бит
{
		if (n >= BitLen || n < 0)
			throw "wrong n";
		pMem[GetMemIndex(n)] &= ~GetMemMask(n);
}

int TBitField::GetBit(const int n) const // получить значение бита
{
		if (n >= BitLen || n < 0)
			throw "wrong n";
		if ((pMem[GetMemIndex(n)] & GetMemMask(n)) == GetMemMask(n))
			return 1;
		else
			return 0;
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{

		BitLen = bf.GetLength();
		MemLen = (BitLen / (sizeof TELEM * 8) + (bool)(BitLen % (sizeof TELEM * 8)));
		pMem = new TELEM[MemLen];
		for (int i = 0; i < MemLen; i++)
			pMem[i] = 0;
		for (int i = 0; i < GetLength(); i++)
			if (bf.GetBit(i))
				SetBit(i);
			else
				ClrBit(i);
		return *this;
}	

int TBitField::operator==(const TBitField &bf) const // сравнение
{
		int result;
		result = 1;
		if (bf.GetLength() == BitLen)
		{
			for (int i = 0; (i < BitLen && result == 1); i++)
				if (GetBit(i) != bf.GetBit(i))
					result = 0;
		}
		else
			result = 0;
		return result;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
		return !(*this == bf);
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
		TBitField result((BitLen >= bf.GetLength()) ? *this : bf);
		for (int i = 0; i < ((BitLen >= bf.GetLength()) ? bf.GetLength() : BitLen); i++)
			if (GetBit(i) | bf.GetBit(i))
				result.SetBit(i);
			else
				result.ClrBit(i);
		return result;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
		TBitField result((BitLen >= bf.GetLength()) ? BitLen : bf.GetLength());
		for (int i = 0; i < ((BitLen >= bf.GetLength()) ? bf.GetLength() : BitLen); i++)
			if (GetBit(i) & bf.GetBit(i))
				result.SetBit(i);
			else
				result.ClrBit(i);
		return result;
}

TBitField TBitField::operator~() // отрицание
{
		TBitField temp(*this);
		for (int i = 0; i < BitLen; i++)
			temp.GetBit(i) ? temp.ClrBit(i) : temp.SetBit(i);
		return temp;
}

// ввод/вывод

istream &operator >> (istream &istr, TBitField &bf) // ввод
{
		char value;
		for (int i = 0; i < bf.BitLen; i++)
		{
			istr >> value;
			if (value == 0)
				bf.ClrBit(i);
			else if (value == 1)
				bf.SetBit(i);
			else
				break;
		}
		return istr;

}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
		for (int i = 0; i < bf.BitLen; i++) 
		{
			ostr << bf.GetBit(i);
		}
		return ostr;

}
```

## Заголовочный файл TSet

```C++
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tset.h - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Множество

#ifndef __SET_H__
#define __SET_H__

#include "tbitfield.h"

class TSet
{
private:
  int MaxPower;       // максимальная мощность множества
  TBitField BitField; // битовое поле для хранения характеристического вектора
public:
  TSet(int mp);
  TSet(const TSet &s);       // конструктор копирования
  TSet(const TBitField &bf); // конструктор преобразования типа
  operator TBitField();      // преобразование типа к битовому полю
  // доступ к битам
  int GetMaxPower(void) const;     // максимальная мощность множества
  void InsElem(const int Elem);       // включить элемент в множество
  void DelElem(const int Elem);       // удалить элемент из множества
  int IsMember(const int Elem) const; // проверить наличие элемента в множестве
  // теоретико-множественные операции
  int operator== (const TSet &s) const; // сравнение
  int operator!= (const TSet &s) const; // сравнение
  TSet& operator=(const TSet &s);  // присваивание
  TSet operator+ (const int Elem); // объединение с элементом
                                   // элемент должен быть из того же универса
  TSet operator- (const int Elem); // разность с элементом
                                   // элемент должен быть из того же универса
  TSet operator+ (const TSet &s);  // объединение
  TSet operator* (const TSet &s);  // пересечение
  TSet operator~ (void);           // дополнение
							  
  friend istream &operator>>(istream &istr, TSet &bf);
  friend ostream &operator<<(ostream &ostr, const TSet &bf);
};
#endif

```

## Реализация класса TSet

```C++
#include "tset.h"

TSet::TSet(int mp) : BitField(mp)
{
	MaxPower = mp;
}

// конструктор копирования
TSet::TSet(const TSet &s) :	BitField(s.BitField)
{
	MaxPower = s.GetMaxPower();
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf)
{
	MaxPower = bf.GetLength();
}

TSet::operator TBitField()
{
	return TBitField(MaxPower);
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
	if (Elem < MaxPower)
		return BitField.GetBit(Elem);
	else
		return 0;
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	BitField.ClrBit(Elem);
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{
	MaxPower = s.MaxPower;
	BitField = s.BitField;
	return *this;
}

int TSet::operator==(const TSet &s) const // сравнение
{
    return ((BitField == s.BitField) && (MaxPower == s.MaxPower));
}

int TSet::operator!=(const TSet &s) const // сравнение
{
	return !operator==(s);
}

TSet TSet::operator+(const TSet &s) // объединение
{
	TSet result(BitField | s.BitField);
	return result;
}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	TSet result(*this);
	result.BitField.SetBit(Elem);
	return result;
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	TSet result(*this);
	result.BitField.ClrBit(Elem);
	return result;
}

TSet TSet::operator*(const TSet &s) // пересечение
{
	TSet result(BitField & s.BitField);
	return result;
}

TSet TSet::operator~(void) // дополнение
{
	return (~BitField);
}


istream &operator>>(istream &istr, TSet &s) // ввод
{
	char ch = ',';
	int Elem;
	for (; ch == ',';)
	{
		istr >> Elem;
		istr >> ch;
		s.InsElem(Elem);
	}
	return istr;
}
ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	ostr << "{ ";
	for (int i = 0; i < s.GetMaxPower(); i++)
		if (s.IsMember(i))
			ostr << 'A' << i << ' ';
	ostr << " }";
	return ostr;
}

```
## Пример работы тестов и решета Эратосфена

![GoogleTests](http://i.imgur.com/1gx6RNK.png)
![Решето Эратосфена](http://i.imgur.com/AOAoArE.png)

## Вывод

  Цели работы достигнуты. Реализованы методы классов "TSet" и "TBitField" и успешно пройдены все тесты основанные на фреймворке GoogleTests, а так же доказана работоспособность классов на основе имеющегося алгоритма поиска простых чисел.
  Мне удалось освоить принцип работы с GoogleTests, улучшить навык работы с классами и ООП в целом. 
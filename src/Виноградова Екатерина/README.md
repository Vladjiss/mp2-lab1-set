##Множества на основе битовых полей

####Цели и задачи

*Цель данной работы* — разработка структуры данных для хранения множеств с использованием битовых полей, а также освоение таких инструментов разработки программного обеспечения, как система контроля версий Git и фрэймворк для разработки автоматических тестов Google Test.

*Выполнение работы предполагает решение следующих задач:*

1.	Реализация класса битового поля `TBitField` согласно заданному интерфейсу.
2.	Реализация класса множества `TSet` согласно заданному интерфейсу.
3.	Обеспечение работоспособности тестов и примера использования.
4.	Реализация нескольких простых тестов на базе Google Test.
5.	Публикация исходных кодов в личном репозитории на BitBucket.

Перед выполнением работы был получен проект-шаблон, содержащий:

- Интерфейсы классов битового поля и множества (h-файлы)
- Готовый набор тестов для каждого из указанных классов
- Пример использования класса битового поля и множества для решения задачи поиска простых чисел с помощью алгоритма "Решето Эратосфена"

####Реализация класса битового поля TBitField

```cpp
#include "tbitfield.h"

#define BYTE 8

TBitField::TBitField(int len)
{
	if (len < 0)
		throw "Negative length";
	BitLen = len;
	MemLen = len / (sizeof(TELEM)*BYTE);
	if (len > MemLen*sizeof(TELEM)*BYTE)
		MemLen++;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	if (pMem != NULL)
		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];
}

TBitField::~TBitField()
{
	delete[] pMem;
	pMem = NULL;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	return (n / (sizeof(TELEM)*BYTE));
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	return (TELEM)(1 << n);
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
  return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if ((n < 0) || (n >= BitLen))
		throw "Uncorrect index";
	pMem[GetMemIndex(n)] |= GetMemMask(n);
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if ((n < 0) || (n >= BitLen))
		throw "Uncorrect index";
	pMem[GetMemIndex(n)] &= ~GetMemMask(n);
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	if ((n < 0) || (n >= BitLen))
		throw "Uncorrect index";
	return pMem[GetMemIndex(n)] & GetMemMask(n);
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	if (*this != bf)
	{
		delete[] pMem;
		pMem = NULL;
		BitLen = bf.BitLen;
		MemLen = bf.MemLen;
		pMem = new TELEM[MemLen];
		if (pMem != NULL)
		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];
	}
	return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	if (BitLen != bf.BitLen)
		return 0;
	for (int i = 0; i < MemLen; i++)
		if (pMem[i] != bf.pMem[i])
			return 0;
	return 1;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
	return !(*this == bf);
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	TBitField tmp(*this);
	if (bf.BitLen > BitLen)
	{
		tmp = bf;
		for (int i = 0; i < MemLen; i++)
			tmp.pMem[i] |= pMem[i];
	}
	else
	for (int i = 0; i < bf.MemLen; i++)
		tmp.pMem[i] |= bf.pMem[i];
	return tmp;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	TBitField tmp(*this);
	if (bf.BitLen > BitLen)
	{
		tmp = bf;
		for (int i = 0; i < MemLen; i++)
			tmp.pMem[i] &= pMem[i];
	}
	else
	for (int i = 0; i < bf.MemLen; i++)
		tmp.pMem[i] &= bf.pMem[i];
	return tmp;
}

TBitField TBitField::operator~(void) // отрицание
{
	TBitField tmp(BitLen);
	for (int i = 0; i < BitLen; i++)
	if (GetBit(i) == 0)
		tmp.SetBit(i);
	return tmp;
}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	char ch;
	for (int i = 0; i < bf.BitLen; i++)
	{
		istr >> ch;
		if (ch == '0')
			bf.ClrBit(i);
		if (ch == '1')
			bf.SetBit(i);
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for (int i = 0; i < bf.BitLen; i++)
		if (bf.GetBit(i))
			ostr << 1;
		else
			ostr << 0;
	ostr << endl;
	return ostr;
}

```

####Реализация класса множества TSet

```cpp
#include "tset.h"

TSet::TSet(int mp) : MaxPower(mp), BitField(mp)
{
}

// конструктор копирования
TSet::TSet(const TSet &s) : MaxPower(s.MaxPower), BitField(s.BitField)
{
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : MaxPower(bf.GetLength()), BitField(bf)
{
}

TSet::operator TBitField()
{
	TBitField tmp(BitField);
	return tmp;
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
    return BitField.GetBit(Elem);
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	BitField.ClrBit(Elem);
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{
	MaxPower = s.MaxPower;
	BitField = s.BitField;
	return *this;
}

int TSet::operator==(const TSet &s) const // сравнение
{
	return BitField == s.BitField;
}

int TSet::operator!=(const TSet &s) const // сравнение
{
	return !(*this == s);
}

TSet TSet::operator+(const TSet &s) // объединение
{
	TSet tmp(BitField | s.BitField);
	return tmp;
}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	TSet tmp(*this);
	tmp.InsElem(Elem);
	return tmp;
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	TSet tmp(*this);
	tmp.DelElem(Elem);
	return tmp;
}

TSet TSet::operator*(const TSet &s) // пересечение
{
	TSet tmp(*this);
	tmp.BitField = (BitField & s.BitField);
	return tmp;
}

TSet TSet::operator~(void) // дополнение
{
	TSet tmp(~BitField);
	return tmp;
}

// перегрузка ввода/вывода

istream &operator>>(istream &istr, TSet &s) // ввод
{
	char ch;
	int Elem = 0, pow = 0;
	istr >> ch;
	while ((ch != '\n') && (pow <= s.MaxPower))
	{
		while (ch != ' ')
		{
			if ((ch >= '0') && (ch <= '9'))
				Elem = Elem * 10 + (ch - '0');
			else
				throw("Uncorrect symbol");
			istr >> ch;
		}
		s.InsElem(Elem);
		Elem = 0;
		pow++;
		istr >> ch;
	}
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	ostr << "{ ";
	for (int i = 0; i < s.MaxPower; i++)
		if (s.IsMember(i))
			ostr << i << ' ';
	ostr << '}' << endl;
	return ostr;
}
```

####Прохождение тестов

![Tests.png](http://s019.radikal.ru/i619/1610/05/0035479a8004.png)

Самостоятельно были добавлены тесты, проверяющие правильность выполнения нескольких операций в одну строку (`bf1 | bf2 | bf3` и `set1 + set2 + set3`)

```cpp
TEST(TBitField, or_operator_applied_three_bitfields_of_equal_size)
{
	const int size = 5;
	TBitField bf1(size), bf2(size), bf3(size), expBf(size);
	// bf1 = 00110
	bf1.SetBit(2);
	bf1.SetBit(3);
	// bf2 = 01010
	bf2.SetBit(1);
	bf2.SetBit(3);
	// bf3 = 00011
	bf3.SetBit(3);
	bf3.SetBit(4);

	// expBf = 01111
	expBf.SetBit(1);
	expBf.SetBit(2);
	expBf.SetBit(3);
	expBf.SetBit(4);

	EXPECT_EQ(expBf, bf1 | bf2 | bf3);
}

TEST(TSet, can_combine_three_sets_of_equal_size)
{
	const int size = 5;
	TSet set1(size), set2(size), set3(size), expSet(size);
	// set1 = {1, 2, 4}
	set1.InsElem(1);
	set1.InsElem(2);
	set1.InsElem(4);
	// set2 = {0, 1, 2}
	set2.InsElem(0);
	set2.InsElem(1);
	set2.InsElem(2);
	// set3 = {0, 2, 4}
	set3.InsElem(0);
	set3.InsElem(2);
	set3.InsElem(4);

	// expSet = {0, 1, 2, 4}
	expSet.InsElem(0);
	expSet.InsElem(1);
	expSet.InsElem(2);
	expSet.InsElem(4);

	EXPECT_EQ(expSet, set1 + set2 + set3);
}
```

####Решето Эратосфена

![Eratosfen.png](http://s018.radikal.ru/i527/1610/03/4fe8bd8ac212.png)

####Вывод

В ходе выполнения данной работы были реализованы классы `TBitField` и `TSet`, причем класс `TSet` реализован на основе `TBitField`.

Также в работе впервые была использована система тестирования Google Test. Наличие тестов позволяет проверить реализацию методов и быстро найти те ошибки, которые легко пропустить. Очевидно, что тестировать необходимо каждый метод, проверять все, что может вызвать ошибку.
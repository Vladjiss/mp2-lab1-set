#include "tbitfield.h"
#define BYTE 8
int GetSlip()//Вспомогательная функция на нахождение сдвига, в зависимости от значения TELEM. 
{
	int size = sizeof(TELEM) * BYTE;
	int count = 0;
	while (size =size >> 1) 
		count++;
	return count;
}
int Slip = GetSlip();
int TBitField::GetMemLenght(const int len)//Вспомогательный метод на нахождение количества элементов(ячеек) массива.
{
	return (len + sizeof(TELEM)*BYTE - 1) >> Slip;
}
TBitField::TBitField(int len) //Конструктор.
{
	if (len > 0)
	{
		BitLen = len;
		MemLen = GetMemLenght(len);
		pMem = new TELEM[MemLen];
		for (int i = 0; i < MemLen; i++)
			pMem[i] = 0;
	}
	else
		throw "Ошибка размера поля";
}
TBitField::TBitField(const TBitField &bf) // Конструктор копирования.
{
	BitLen = bf.GetLength();
	MemLen = GetMemLenght(BitLen);
	pMem = new TELEM[MemLen];
	for (int i = 0; i < BitLen; i++)
	{
		if (bf.GetBit(i))
			SetBit(i);
		else
			ClrBit(i);
	}
}
TBitField::~TBitField()//Деструктор.
{
	delete[] pMem;
}
int TBitField::GetMemIndex(const int n) const // Индекс Mem для бита n.
{
	if (n >= BitLen || n < 0)
		throw "Выход за границы битового поля";
	return n >> Slip;
}
TELEM TBitField::GetMemMask(const int n) const // Битовая маска для бита n.
{
	if (n >= BitLen || n < 0)
		throw "Выход за границы битового поля";
	return 1 << (n - GetMemIndex(n) * sizeof(TELEM)*BYTE);
}
// доступ к битам битового поля
int TBitField::GetLength(void) const // Получить количество битов.
{
	return BitLen;
}
void TBitField::SetBit(const int n) // Установить бит (1).
{
	if (n >= BitLen || n < 0)
		throw "Выход за границы битового поля";
	pMem[GetMemIndex(n)] |= GetMemMask(n);
}
void TBitField::ClrBit(const int n) // Очистить бит (0)
{
	if (n >= BitLen || n < 0)
		throw "Выход за границы битового поля";
	pMem[GetMemIndex(n)] &= ~GetMemMask(n);
}
int TBitField::GetBit(const int n) const // Узнать значение бита.
{
	if (n >= BitLen || n < 0)
		throw "Выход за границы битового поля";
	return pMem[GetMemIndex(n)] & GetMemMask(n);
}
// битовые операции
TBitField& TBitField::operator=(const TBitField &bf) // Оператор присваивания
{
	if (pMem == NULL) 
		throw "Ошибка. Объект не существует";
	if (this != &bf) 
	{
		delete[] pMem;
		BitLen = bf.GetLength();
		MemLen = GetMemLenght(BitLen);
		pMem = new TELEM[MemLen];
		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];
		return *this;

	}
	return *this;
}
int TBitField::operator==(const TBitField &bf) const // Оператор сравнения (Равенства).
{
	if (GetLength() == bf.GetLength())
	{
		for (int i = 0; i < BitLen; i++)
		{
			if (GetBit(i) != bf.GetBit(i))
				return 0;
		}
	}
	else
		return 0;
	return 1;
}
int TBitField::operator!=(const TBitField &bf) const // Оператор сравнения (Неравенства).
{
	return !operator==(bf);
}
TBitField TBitField::operator|(const TBitField &bf) // Оператор "или".
{
	TBitField tempBitField((BitLen > bf.BitLen) ? BitLen : bf.BitLen);
	for (int i = 0; i < MemLen; i++)
		tempBitField.pMem[i] = pMem[i];
	for (int i = 0; i < bf.MemLen; i++)
		tempBitField.pMem[i] = tempBitField.pMem[i] | bf.pMem[i];
	return tempBitField;
}
TBitField TBitField::operator&(const TBitField &bf) // Оператор "и".
{
	TBitField tempBitField((BitLen > bf.BitLen) ? BitLen : bf.BitLen);
	for (int i = 0; i < MemLen; i++)
		tempBitField.pMem[i] = pMem[i];
	for (int i = 0; i < bf.MemLen; i++)
		tempBitField.pMem[i] = tempBitField.pMem[i] & bf.pMem[i];
	return tempBitField;
}
TBitField TBitField::operator~(void) // Дополнение
{
	TBitField tempBitField(GetLength());
	for (int i = 0; i < BitLen; i++)
	{
		if (GetBit(i)) 
			tempBitField.ClrBit(i);
		else 
			tempBitField.SetBit(i);
	}
	return tempBitField;
}
// Ввод/вывод
istream &operator >> (istream &istr, TBitField &bf) // Ввод.
{
	char InValue;
	int i = 0;
	while (true)
	{
		istr >> InValue;
		if (i < bf.GetLength())
		{
			if (InValue == '1')
				bf.SetBit(i);
			else
				if (InValue == '0')
					bf.ClrBit(i);
				else
					break;
		}
		else
			break;
		i++;
	}
	return istr;
}
ostream &operator<<(ostream &ostr, const TBitField &bf) // Вывод.
{
	for (int i = 0; i < bf.GetLength(); i++)
		if (bf.GetBit(i))
			ostr << 1;
		else
			ostr << 0;
	return ostr;
}
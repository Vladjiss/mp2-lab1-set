// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

/*
* Список исключений:
* 1 - отрицательная длина битового поля
* 2 - не удалось выделить память для создания битового поля
* 3 - выход за пределы битового поля (бит с указанным номером не существует)
*/

#include "tbitfield.h"
#define BITS_IN_BYTE 8
#define ELEMENTS_IN_TELEM sizeof(TELEM)*BITS_IN_BYTE

TBitField::TBitField(int len) : BitLen(len)
{

	if (len < 0) {
		throw 1;
	}

	MemLen = len / ELEMENTS_IN_TELEM + (bool)(len % ELEMENTS_IN_TELEM);

	pMem = new TELEM[MemLen];

	if (pMem == nullptr) {
		throw 2;
	}

	for (int i = 0; i < MemLen; i++) {
		pMem[i] = 0;
	}

}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{

	BitLen = bf.BitLen;
	MemLen = bf.MemLen;

	pMem = new TELEM[MemLen];

	if (pMem == nullptr) {
		throw 2;
	}

	for (int i = 0; i < MemLen; i++) {
		pMem[i] = bf.pMem[i];
	}

}

TBitField::~TBitField()
{

	delete[] pMem;

}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{

	if (n < 0 || n >= BitLen) {
		throw 3;
	}

	return n / ELEMENTS_IN_TELEM;

}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{

	if (n < 0 || n >= BitLen) {
		throw 3;
	}

	return 1 << (n - GetMemIndex(n)*ELEMENTS_IN_TELEM);

}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
	
	return BitLen;

}

void TBitField::SetBit(const int n) // установить бит
{

	if (n < 0 || n >= BitLen) {
		throw 3;
	}

	pMem[GetMemIndex(n)] |= GetMemMask(n);

}

void TBitField::ClrBit(const int n) // очистить бит
{

	if (n < 0 || n >= BitLen) {
		throw 3;
	}

	pMem[GetMemIndex(n)] &= ~GetMemMask(n);

}

int TBitField::GetBit(const int n) const // получить значение бита
{

	if (n < 0 || n >= BitLen) {
		throw 3;
	}

	return (bool)(pMem[GetMemIndex(n)] & GetMemMask(n));

}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{

	if (&bf != this) {

		delete[] pMem;

		BitLen = bf.BitLen;
		MemLen = bf.MemLen;

		pMem = new TELEM[MemLen];

		if (pMem == nullptr) {
			throw 2;
		}

		for (int i = 0; i < MemLen; i++) {
			pMem[i] = bf.pMem[i];
		}

	} // if (&bf != this)

	return *this;

}

int TBitField::operator==(const TBitField &bf) const // сравнение
{

	bool equiv = true;

	if (BitLen != bf.BitLen) {
		equiv = false;
	}

	for (int i = 0; i < BitLen; i++) {
		if (GetBit(i) != bf.GetBit(i)) {
			equiv = false;
		}
	}

	return (int)equiv;

}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
  
	return !(*this == bf);

}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{

	TBitField temp((bf.BitLen > BitLen) ? bf.BitLen : BitLen);

	for (int i = 0; i < MemLen; i++) {
		temp.pMem[i] = pMem[i] | bf.pMem[i];
	}

	return temp;

}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{

	TBitField temp((bf.BitLen > BitLen) ? bf.BitLen : BitLen);

	for (int i = 0; i < MemLen; i++) {
		temp.pMem[i] = pMem[i] & bf.pMem[i];
	}

	return temp;

}

TBitField TBitField::operator~(void) // отрицание
{

	TBitField temp(BitLen);

	for (int i = 0; i < BitLen; i++) {

		if (GetBit(i))
			temp.ClrBit(i);
		else
			temp.SetBit(i);

	}

	return temp;

}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{

	char curr_bit_value;

	for (int i = 0; i < bf.BitLen; i++) {

		istr >> curr_bit_value;

		if (curr_bit_value == 0)
			bf.ClrBit(i);
		else if (curr_bit_value == 1)
			bf.SetBit(i);
		else
			break;

	}

	return istr;

}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{

	for (int i = 0; i < bf.BitLen; i++) {

		ostr << bf.GetBit(i);


	}

	return ostr;

}

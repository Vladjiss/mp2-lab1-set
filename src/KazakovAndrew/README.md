# Отчёт по выполненной работе "Множества на основе битовых полей"
## Введение

**Цель данной работы** — разработка структуры данных для хранения множеств с использованием битовых полей, а также освоение таких инструментов разработки программного обеспечения, как система контроля версий [Git][git] и фреймворк для разработки автоматических тестов [Google Test][gtest].

Для разработки был использован язык C++.

## Необходимые формальности
Начало каждого заголовочного файла и файла с реализацией
```C++
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.h / tbitfield.cpp / tset.h / tset.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
```

## Структура данных "битовое поле"
Реализация структуры данных "битовое поле" целиком содержится в классе TBitField. Само по себе битовое поле представляется в виде массива из элементов типа `int`, поскольку во внутреннем представлении массив - это вектор из нулей и единиц.

Работа непосредственно с битами идёт через битовые маски.

### Заголовочный файл класса TBitField
```C++
#ifndef __BITFIELD_H__
#define __BITFIELD_H__

#include <iostream>

using namespace std;

typedef unsigned int TELEM;

class TBitField
{
private:
  int  BitLen; // длина битового поля - макс. к-во битов
  TELEM *pMem; // память для представления битового поля
  int  MemLen; // к-во эл-тов Мем для представления бит.поля
  int   GetMemIndex(const int n) const; // индекс в pМем для бита n
  TELEM GetMemMask (const int n) const; // битовая маска для бита n

public:
  TBitField(int len);
  TBitField(const TBitField &bf);
  ~TBitField();

  int GetLength(void) const;      // получить длину (к-во битов)
  void SetBit(const int n);       // установить бит
  void ClrBit(const int n);       // очистить бит
  int  GetBit(const int n) const; // получить значение бита

  // Битовые операции
  int operator==(const TBitField &bf) const;
  int operator!=(const TBitField &bf) const;
  TBitField& operator=(const TBitField &bf);
  TBitField  operator|(const TBitField &bf);
  TBitField  operator&(const TBitField &bf);
  TBitField  operator~(void);
  
  // Ввод-вывод
  friend istream &operator>>(istream &istr, TBitField &bf);
  friend ostream &operator<<(ostream &ostr, const TBitField &bf);
};
```

### Реализация класса TBitField
#### Исключения, выбрасываемые классом TBitField
Возникающие в результате работы программы ошибки не будем обрабатывать конкретно, а будем выбрасывать исключения со следующими кодами:
* **1** - "отрицательная длина битового поля";
* **2** - "не удалось выделить память для создания битового поля";
* **3** - "выход за пределы битового поля (бит с указанным номером не существует)".

#### Начало файла

В начале файла, как всегда, подключаем заголовочный файл и определяем константы, которые в дальнейшем будем использовать в методах класса для машинонезависимой реализации.

```C++
#include "tbitfield.h"
#define BITS_IN_BYTE 8
#define ELEMENTS_IN_TELEM sizeof(TELEM)*BITS_IN_BYTE
```

#### Конструктор по умолчанию
Как видно, тут мы и будем использовать константу `ELEMENTS_IN_TELEM`. Не забываем обнулять массив.

```C++
TBitField::TBitField(int len) : BitLen(len)
{

	if (len < 0) {
		throw 1;
	}

	MemLen = len / ELEMENTS_IN_TELEM + (bool)(len % ELEMENTS_IN_TELEM);

	pMem = new TELEM[MemLen];

	if (pMem == nullptr) {
		throw 2;
	}

	for (int i = 0; i < MemLen; i++) {
		pMem[i] = 0;
	}

}
```

#### Конструтор копирования

```C++
TBitField::TBitField(const TBitField &bf)
{

	BitLen = bf.BitLen;
	MemLen = bf.MemLen;

	pMem = new TELEM[MemLen];

	if (pMem == nullptr) {
		throw 2;
	}

	for (int i = 0; i < MemLen; i++) {
		pMem[i] = bf.pMem[i];
	}

}
```

#### Деструктор

```C++
TBitField::~TBitField()
{

	delete[] pMem;

}
```

#### Индекс `Мем` для бита `n`

```C++
int TBitField::GetMemIndex(const int n) const
{

	if (n < 0 || n >= BitLen) {
		throw 3;
	}

	return n / ELEMENTS_IN_TELEM;

}
```

#### Битовая маска для бита `n`

```C++
TELEM TBitField::GetMemMask(const int n) const
{

	if (n < 0 || n >= BitLen) {
		throw 3;
	}

	return 1 << (n - GetMemIndex(n)*ELEMENTS_IN_TELEM);

}
```

#### Основные операции с битами
```C++
// Получение длины битового поля (кол-ва битов)
int TBitField::GetLength(void) const
{
	
	return BitLen;

}

// Установка бита n
void TBitField::SetBit(const int n)
{

	if (n < 0 || n >= BitLen) {
		throw 3;
	}

	pMem[GetMemIndex(n)] |= GetMemMask(n);

}

// Очистка бита n
void TBitField::ClrBit(const int n)
{

	if (n < 0 || n >= BitLen) {
		throw 3;
	}

	pMem[GetMemIndex(n)] &= ~GetMemMask(n);

}

// Получение значения бита n
int TBitField::GetBit(const int n) const
{

	if (n < 0 || n >= BitLen) {
		throw 3;
	}

	return (bool)(pMem[GetMemIndex(n)] & GetMemMask(n));

}
```

#### Присваивание
```C++
TBitField& TBitField::operator=(const TBitField &bf)
{

	if (&bf != this) {

		delete[] pMem;

		BitLen = bf.BitLen;
		MemLen = bf.MemLen;

		pMem = new TELEM[MemLen];

		if (pMem == nullptr) {
			throw 2;
		}

		for (int i = 0; i < MemLen; i++) {
			pMem[i] = bf.pMem[i];
		}

	} // if (&bf != this)

	return *this;

}
```

#### Сравнение
Два битовых поля равны тогда и только тогда, когда совпадают их размеры и значения соответствующих битов.
```C++
int TBitField::operator==(const TBitField &bf) const
{

	bool equiv = true;

	if (BitLen != bf.BitLen) {
		equiv = false;
	}

	for (int i = 0; i < BitLen; i++) {
		if (GetBit(i) != bf.GetBit(i)) {
			equiv = false;
		}
	}

	return (int)equiv;
}

int TBitField::operator!=(const TBitField &bf) const 
{
  
	return !(*this == bf);

}
```

#### Битовые операции
```C++
// Операция ИЛИ
TBitField TBitField::operator|(const TBitField &bf)
{

	TBitField temp((bf.BitLen > BitLen) ? bf.BitLen : BitLen);

	for (int i = 0; i < MemLen; i++) {
		temp.pMem[i] = pMem[i] | bf.pMem[i];
	}

	return temp;

}

// Операция И
TBitField TBitField::operator&(const TBitField &bf)
{

	TBitField temp((bf.BitLen > BitLen) ? bf.BitLen : BitLen);

	for (int i = 0; i < MemLen; i++) {
		temp.pMem[i] = pMem[i] & bf.pMem[i];
	}

	return temp;

}

// Отрицание
TBitField TBitField::operator~(void)
{

	TBitField temp(BitLen);

	for (int i = 0; i < BitLen; i++) {

		if (GetBit(i))
			temp.ClrBit(i);
		else
			temp.SetBit(i);

	}

	return temp;

}
```

#### Ввод и вывод
```C++
// Ввод
istream &operator>>(istream &istr, TBitField &bf)
{

	char curr_bit_value;

	for (int i = 0; i < bf.BitLen; i++) {

		istr >> curr_bit_value;

		if (curr_bit_value == 0)
			bf.ClrBit(i);
		else if (curr_bit_value == 1)
			bf.SetBit(i);
		else
			break;

	}

	return istr;

}

// Вывод
ostream &operator<<(ostream &ostr, const TBitField &bf)
{

	for (int i = 0; i < bf.BitLen; i++) {

		ostr << bf.GetBit(i);


	}

	return ostr;

}
```

### Тесты для проверки корректности реализации, написанные для фреймворка Google Test

Изначально были предоставлены некоторые уже готовые тесты; также были написаны дополнительные тесты. Все они описаны в файле `test/test_tbitfield.cpp`.

Данная реализация успешно справилась с прохождением всех тестов (см. рис. 1 в конце данного файла).

## Использование класса битового поля для реализации класса "Множество"

Класс "битовое поле" очень удобно использовать для реализации обыкновенного математического множества, состоящего из целых неотрицательных чисел: элемент `i` принадлежит множеству тогда и только тогда, когда соответствующий бит установлен в "1". Отсюда вытекает следующая реализация класса "множество" (TSet).

### Заголовочный файл класса TSet

```C++
#ifndef __SET_H__
#define __SET_H__

#include "tbitfield.h"

class TSet
{
private:
  int MaxPower;       // максимальная мощность множества
  TBitField BitField; // битовое поле для хранения характеристического вектора

public:
  TSet(int mp);
  TSet(const TSet &s);       // конструктор копирования
  TSet(const TBitField &bf); // конструктор преобразования типа
  operator TBitField();      // преобразование типа к битовому полю
  
  // Доступ к битам
  int GetMaxPower(void) const;     // максимальная мощность множества
  void InsElem(const int Elem);       // включить элемент в множество
  void DelElem(const int Elem);       // удалить элемент из множества
  int IsMember(const int Elem) const; // проверить наличие элемента в множестве
  
  // Теоретико-множественные операции
  int operator== (const TSet &s) const; // сравнение
  int operator!= (const TSet &s) const; // сравнение
  TSet& operator=(const TSet &s);  // присваивание
  TSet operator+ (const int Elem); // объединение с элементом
                                   // элемент должен быть из того же универса
  TSet operator- (const int Elem); // разность с элементом
                                   // элемент должен быть из того же универса
  TSet operator+ (const TSet &s);  // объединение
  TSet operator* (const TSet &s);  // пересечение
  TSet operator~ (void);           // дополнение

  friend istream &operator>>(istream &istr, TSet &bf);
  friend ostream &operator<<(ostream &ostr, const TSet &bf);
};
#endif
```

### Реализация класса TSet
Поскольку класс `TSet` полностью сделан на основе класса `TBitField`, в реализации имеем минимум кода.

Кстати, при использовании `TBitField` реализация объединения, пересечения и дополнения множества становится очень естественной.

```C++
#include "tset.h"

TSet::TSet(int mp) : BitField(mp)
{

	MaxPower = mp;

}

// конструктор копирования
TSet::TSet(const TSet &s) : BitField(s.BitField)
{

	MaxPower = s.MaxPower;

}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf)
{

	MaxPower = bf.GetLength();

}

TSet::operator TBitField()
{

	return BitField;

}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{

	return MaxPower;

}

int TSet::IsMember(const int Elem) const // элемент множества?
{

	return BitField.GetBit(Elem);

}

void TSet::InsElem(const int Elem) // включение элемента множества
{

	BitField.SetBit(Elem);

}

void TSet::DelElem(const int Elem) // исключение элемента множества
{

	BitField.ClrBit(Elem);

}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{

	MaxPower = s.MaxPower;
	BitField = s.BitField;

	return *this;

}

int TSet::operator==(const TSet &s) const // сравнение
{
    
	bool equiv = false;
	if (BitField == s.BitField)
		equiv = true;

	return (int)equiv;

}

int TSet::operator!=(const TSet &s) const // сравнение
{

	return !(*this == s);

}

TSet TSet::operator+(const TSet &s) // объединение
{

	// Можно так делать, т. к. есть конструктор преобразования типа
	TSet temp = BitField | s.BitField;

	return temp;

}

TSet TSet::operator+(const int Elem) // объединение с элементом
{

	TSet temp = *this;
	temp.InsElem(Elem);

	return temp;

}

TSet TSet::operator-(const int Elem) // разность с элементом
{

	TSet temp = *this;
	temp.DelElem(Elem);

	return temp;

}

TSet TSet::operator*(const TSet &s) // пересечение
{

	TSet temp = BitField & s.BitField;

	return temp;

}

TSet TSet::operator~(void) // дополнение
{

	TSet temp = ~BitField;

	return temp;

}

// перегрузка ввода/вывода

// Ввод
istream &operator>>(istream &istr, TSet &s)
{

	char delimeter;
	int curr_elem;

	while (true) {

		istr >> curr_elem;
		s.InsElem(curr_elem);

		istr >> delimeter;
		if (delimeter != ',')
			break;

	}

	return istr;

}

// Вывод
ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{

	bool is_this_first = true;

	ostr << "{";

	for (int i = 0; i < s.MaxPower; i++) {

		if (s.IsMember(i)) {

			if (!is_this_first)
				ostr << ", ";
			else
				is_this_first = false;

			ostr << "i";

		}

	} // Конец for

	ostr << "}";

	return ostr;

}

```

### Тесты для проверки корректности реализации, написанные для фреймворка Google Test

Тесты для класса `TSet` также были написаны и успешно пройдены (см. рис. 1 в конце данного файла).

## Использование класса TBitField (TSet) для поиска простых чисел с помощью алгоритма "решето Эратосфена"
Использование любого из данных классов хорошо скажется на экономии памяти, поскольку будет использован каждый бит предоставляемого массива.

Результат выполнения реализации "решета" см. на рис. 2 в конце данного файла.

```C++
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// sample_prime_numbers.cpp - Copyright (c) Гергель В.П. 20.08.2000
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Тестирование битового поля и множества

#include <iomanip>

// #define USE_SET // Использовать класс TSet,
                // закоментировать, чтобы использовать битовое поле

#ifndef USE_SET // Использовать класс TBitField

#include "tbitfield.h"

int main()
{
  int n, m, k, count;

  setlocale(LC_ALL, "Russian");
  cout << "Тестирование программ поддержки битового поля" << endl;
  cout << "             Решето Эратосфена" << endl;
  cout << "Введите верхнюю границу целых значений - ";
  cin  >> n;
  TBitField s(n + 1);
  // заполнение множества
  for (m = 2; m <= n; m++)
    s.SetBit(m);
  // проверка до sqrt(n) и удаление кратных
  for (m = 2; m * m <= n; m++)
    // если m в s, удаление кратных
    if (s.GetBit(m))
      for (k = 2 * m; k <= n; k += m)
        if (s.GetBit(k))
          s.ClrBit(k);
  // оставшиеся в s элементы - простые числа
  cout << endl << "Печать множества некратных чисел" << endl << s << endl;
  cout << endl << "Печать простых чисел" << endl;
  count = 0;
  k = 1;
  for (m = 2; m <= n; m++)
    if (s.GetBit(m))
    {
      count++;
      cout << setw(3) << m << " ";
      if (k++ % 10 == 0)
        cout << endl;
    }
  cout << endl;
  cout << "В первых " << n << " числах " << count << " простых" << endl;
}
#else

#include "tset.h"

int main()
{
  int n, m, k, count;

  setlocale(LC_ALL, "Russian");
  cout << "Тестирование программ поддержки множества" << endl;
  cout << "              Решето Эратосфена" << endl;
  cout << "Введите верхнюю границу целых значений - ";
  cin  >> n;
  TSet s(n + 1);
  // заполнение множества
  for (m = 2; m <= n; m++)
    s.InsElem(m);
  // проверка до sqrt(n) и удаление кратных
  for (m = 2; m * m <= n; m++)
    // если м в s, удаление кратных
    if (s.IsMember(m))
      for (k = 2 * m; k <= n; k += m)
       if (s.IsMember(k))
         s.DelElem(k);
  // оставшиеся в s элементы - простые числа
  cout << endl << "Печать множества некратных чисел" << endl << s << endl;
  cout << endl << "Печать простых чисел" << endl;
  count = 0;
  k = 1;
  for (m = 2; m <= n; m++)
    if (s.IsMember(m))
    {
      count++;
      cout << setw(3) << m << " ";
      if (k++ % 10 == 0)
        cout << endl;
    }
  cout << endl;
  cout << "В первых " << n << " числах " << count << " простых" << endl;
}

#endif
```

## Приложения

#### Рис. 1. Успешное выполнение тестов для классов TBitField, TSet

![Успешное выполнение тестов для классов TBitField, TSet](1.png)

#### Рис. 2. Работа алгоритма "решето Эратосфена" для поиска простых чисел среди первых 500

![Работа алгоритма "решето Эратосфена" для поиска простых чисел среди первых 500](2.png)

## Выводы
Исключительно благодаря мощности и гибкости языка C++ была написана реализация структуры данных "битовое поле", которая с первого взгляда кажется нелёгкой в реализации и неудобной в представлении в памяти. Реализация включала в себя всевозможные перегрузки различных очевидных и нужных операций над объектами классов `TBitField` и `TSet`, которые упростили работу с объектами.

С другой стороны, поскольку реализация многих перегрузок зависит от реализации других перегрузок, а те реализации снова зачастую от чего-нибудь зависят, отследить ошибки и проверить корректность работы классов - практически невыполнимая задача даже для проекта данного масштаба. Фреймворки наподобие Google Test сильно спасают ситуацию, значительно упрощая тестирование написанного кода. Это, безусловно, полезный и важный инструмент для многих разработчиков, на плечах которых лежит большая ответственность за работоспособность написанных ими программ.

Google Test сильно помог в успешной реализации данного проекта - были быстро найдены проблемные места в коде.

<!-- LINKS -->

[git]:         https://git-scm.com/book/ru/v2
[gtest]:       https://github.com/google/googletest


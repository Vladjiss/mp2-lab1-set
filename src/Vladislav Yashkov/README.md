﻿# Методы программирования 2: Множества на основе битовых полей

## Цели и задачи

__Цель данной работы__  — разработка структуры данных для хранения множеств с
использованием битовых полей, а также освоение таких инструментов разработки
программного обеспечения, как система контроля версий [Git][git] и фрэймворк для
разработки автоматических тестов [Google Test][gtest].

Перед выполнением работы был получен проект-шаблон, содержащий следующее:

 - Интерфейсы классов битового поля и множества (h-файлы)
 - Готовый набор тестов для каждого из указанных классов
 - Пример использования класса битового поля и множества для решения задачи
   поиска простых чисел с помощью алгоритма ["Решето Эратосфена"][sieve]

Выполнение работы предполагает решение следующих задач:

  1. Реализация класса битового поля `TBitField` согласно заданному интерфейсу.
  1. Реализация класса множества `TSet` согласно заданному интерфейсу.
  1. Обеспечение работоспособности тестов и примера использования.
  1. Реализация нескольких простых тестов на базе Google Test.
  1. Публикация исходных кодов в личном репозитории на GitHub.

# Описание класса `TBitField`:
Битовое поле представленно в виде массива pMem с типом TELEM (unsigned int)
BitLen - количество битов в нашем поле,на основе BitLen вычисляется MemLen - количество элементов pMem.
В классе реализованны методы доступа к отдельным битам, операции над битовыми полями и 
операторы ввода/выводы.
Нумерация битов идет справа налево,первый бит имеет индекс (0),а последний (Bitlen-1).

Работа программы предполагает обработку исключений:
1 – отрицательная длина битового поля. 
2 – выход за рамки битового поля. 
3 – обращение к несуществующему элементу. 

####Реализация класса битового поля `TBitField`
```cpp
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#include "tbitfield.h"

int telem_len(){
	return sizeof(TELEM)* 8;
}
int shift(){
	int tmp = telem_len();
	int counter = 0;
	while (tmp >>= 1)
		counter++;
	return counter;
}

TBitField::TBitField(int len)
{
	if (len < 0)
		throw 1;
	BitLen = len;
	MemLen = (BitLen + telem_len() - 1) >> shift();
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	if (bf.pMem != NULL)
		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];
	else
		throw 3;
}
TBitField::~TBitField()
{
	delete[] pMem;
	BitLen = 0;
	MemLen = 0;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if (n >= BitLen || n < 0)
		throw 2;
	return n >> shift();
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if (n >= BitLen || n < 0)
		throw 2;
	return 1 << (n - GetMemIndex(n)*telem_len());
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
	return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if (n >= BitLen || n < 0)
		throw 2;
	pMem[GetMemIndex(n)] |= GetMemMask(n);
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if (n >= BitLen || n < 0)
		throw 2;
	pMem[GetMemIndex(n)] &= ~GetMemMask(n);
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	return (bool)(pMem[GetMemIndex(n)] & GetMemMask(n));
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	delete[] pMem;
	pMem = new TELEM[MemLen];
	if (bf.pMem != NULL)
		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];
	else
		throw 3;
	return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	if (BitLen == bf.BitLen){
		for (int i = 0; i < BitLen; i++)
			if (GetBit(i) != bf.GetBit(i)){
				return 0;
			}
		return 1;
	}
	return 0;
}
int TBitField::operator!=(const TBitField &bf) const // сравнение
{
	return !(*this == bf);
}
TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	TBitField tmp(BitLen > bf.BitLen ? BitLen : bf.BitLen);
	for (int i = 0; i < MemLen; i++)
		tmp.pMem[i] = pMem[i];
	for (int i = 0; i < bf.MemLen; i++)
		tmp.pMem[i] |= bf.pMem[i];
	return tmp;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	TBitField tmp(BitLen > bf.BitLen ? BitLen : bf.BitLen);
	for (int i = 0; i < MemLen; i++)
		tmp.pMem[i] = pMem[i];
	for (int i = 0; i < tmp.MemLen; i++)
	if (i<bf.MemLen)
		tmp.pMem[i] &= bf.pMem[i];
	else
		tmp.pMem[i] = 0;
	return tmp;
}
TBitField TBitField::operator~(void) // отрицание
{
	TBitField tmp(BitLen);
	for (int i = 0; i < MemLen; i++)
		tmp.pMem[i] = ~pMem[i];
	return tmp;
}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	char in;
	int i = 0;
	while (true)
	{
		istr >> in;
		if (i < bf.BitLen)
		{
			if (in == '1')
				bf.SetBit(i);
			else
			if (in == '0')
				bf.ClrBit(i);
			else
				break;
		}
		else
			break;
		i++;
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for (int i = 0; i < bf.BitLen; i++)
		if (bf.GetBit(i))
			ostr << 1;
		else
			ostr << 0;
	return ostr;
}
```

#Описание класса `TSet` - множества на основе битовых полей. 
MaxPowr - максимальная мощность множества, BitField - экземпляр класса `TBitField`. В классе реализованы методы работы с элементами множества, оператор преабразования типа, операторы ввода/вывода.

Класс `TSet` наследует все исключения класса `TBitField`.

####Реализация класса множества `TSet`
```cpp
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tset.cpp - Copyright (c) Гергель В.П. 04.10.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Множество - реализация через битовые поля

#include "tset.h"

TSet::TSet(int mp) : BitField(mp)
{
	MaxPower = mp;
}

// конструктор копирования
TSet::TSet(const TSet &s) : BitField(s.BitField)
{
	MaxPower = s.MaxPower;
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf)
{
	MaxPower = bf.GetLength();
}

TSet::operator TBitField()
{
	return BitField;
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
	return BitField.GetBit(Elem);
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	BitField.ClrBit(Elem);
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{
	BitField = s.BitField;
	MaxPower = s.MaxPower;
	return *this;
}

int TSet::operator==(const TSet &s) const // сравнение
{
	return (MaxPower == s.MaxPower) & (BitField == s.BitField);
}

int TSet::operator!=(const TSet &s) const // сравнение
{
	return !(*this == s);
}

TSet TSet::operator+(const TSet &s) // объединение
{
	TSet tmp = (BitField | s.BitField);
	return tmp;
}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	TSet tmp = *this;
	tmp.BitField.SetBit(Elem);
	return tmp;
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	TSet tmp = *this;
	tmp.BitField.ClrBit(Elem);
	return tmp;
}

TSet TSet::operator*(const TSet &s) // пересечение
{
	return (BitField & s.BitField);
}

TSet TSet::operator~(void) // дополнение
{
	return (~BitField);
}

// перегрузка ввода/вывода

//формат ввода: A1,A3,A4.
istream &operator>>(istream &istr, TSet &s) // ввод
{
	char in;
	int i;
	while (true)
	{
		istr >> in;
		istr >> i;
		s.InsElem(i);
		istr >> in;
		if (in == '.') break;
	}
	return istr;
}

//формат вывода: { A1 A2 ... An }
ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	ostr << "{ ";
	for (int i = 0; i<s.MaxPower; i++)
	if (s.IsMember(i))
		ostr << 'A' << i << ' ';
	ostr << "}";
	return ostr;
}
```

####Скриншоты прохождения тестов
![](https://pp.vk.me/c626827/v626827387/2529e/g4vf3XozQqU.jpg)

####Решето Эратосфена
![](https://pp.vk.me/c626827/v626827387/252b0/P3wHpN1zM_s.jpg)

####Вывод

В результате проделанной работы был реализован класс `TBitField` и на его основе класс `TSet`. Для этого были 
исользованы некоторые возможности ООП (перегрузка операций, дружественные функции, наследование).

Для тестирования работоспособности данных классов, а также выявления ошибок использовалась система Google Test, были успешно пройдены все тесты. Система помогает быстро найти ошибки в коде, для каждого отдельного метода необходима реализация отдельного теста.

[git]:         https://git-scm.com/book/ru/v2
[gtest]:       https://github.com/google/googletest
[sieve]:       http://habrahabr.ru/post/91112
[travis]:      https://travis-ci.org/UNN-VMK-Software/mp2-lab1-set
[git-guide]:   https://bitbucket.org/ashtan/mp2-lab1-set/src/ff6d76c3dcc2a531cefdc17aad5484c9bb8b47c5/docs/part1-git.md?at=master&fileviewer=file-view-default
[gtest-guide]: https://bitbucket.org/ashtan/mp2-lab1-set/src/ff6d76c3dcc2a531cefdc17aad5484c9bb8b47c5/docs/part2-google-test.md?at=master&fileviewer=file-view-default
[youtube-playlist]: https://www.youtube.com/playlist?list=PLSzOhsr5tmhrgV7u7CSzX4Ki1a9r0AKzV
[slides]:      https://bitbucket.org/ashtan/mp2-lab1-set/src/ff6d76c3dcc2a531cefdc17aad5484c9bb8b47c5/docs/slides/?at=master
[upstream]:    https://bitbucket.org/ashtan/mp2-lab1-set

#include "tbitfield.h"
#include <gtest.h>

TEST(Demo, test_test)
{
	EXPECT_EQ(2, 2);
}

TEST(Demo, can_create_bitfield_with_size_1000)
{
	ASSERT_NO_THROW(TBitField bf(1000));
}

TEST(Demo, can_create_bitfield_with_size_minus_1000)
{
	ASSERT_ANY_THROW(TBitField bf(-1000));
}

TEST(Demo, can_set_bit_5)
{
	TBitField bf(10);
	EXPECT_EQ(0, bf.GetBit(5));
	bf.SetBit(5);
	EXPECT_NE(0, bf.GetBit(5));
}

class Parameterized_TBitField_Demo : public::testing::TestWithParam<int>
{
};

TEST_P(Parameterized_TBitField_Demo, can_create_bitfield_with_different_len)
{
	int len = GetParam();

	TBitField bf(len);

	EXPECT_EQ(len, bf.GetLength());
}

INSTANTIATE_TEST_CASE_P(Even,
	Parameterized_TBitField_Demo, 
	::testing::Values(2, 4, 8));

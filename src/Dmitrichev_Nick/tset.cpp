// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tset.cpp - Copyright (c) Гергель В.П. 04.10.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Множество - реализация через битовые поля
// Все исключения заимствуются из tbitfield.h

#include "tset.h"

TSet::TSet(int mp) : MaxPower(mp), BitField(mp)//using the constructor from tbitfield.h
{
}

// конструктор копирования
TSet::TSet(const TSet &s) : MaxPower(s.MaxPower),BitField(s.BitField)//using the constructor from tbitfield.h
{
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf),MaxPower(bf.GetLength())//using the constructor and method from tbitfield.h
{
}

TSet::operator TBitField()
{
	TBitField temp(BitField);
	return temp;
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
    return (BitField.GetBit(Elem)!=0);//using the method from tbitfield.h
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	BitField.SetBit(Elem);//using the method from tbitfield.h
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	BitField.ClrBit(Elem);//using the method from bitfield.h
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{
	BitField = s.BitField;
	MaxPower = s.MaxPower;
	return *this;
}

int TSet::operator==(const TSet &s) const // сравнение
{
	if (MaxPower == s.MaxPower)
		if (BitField == s.BitField)//using the method from tbitfield.h
			return 1;
		else
			return 0;
	else
		return 0;
}

int TSet::operator!=(const TSet &s) const // сравнение
{
	return !(*this == s);
}

TSet TSet::operator+(const TSet &s) // объединение
{
	TSet temp(*this);
	temp.BitField = temp.BitField | s.BitField;//using the method from tbitfield.h
	temp.MaxPower = (MaxPower > s.MaxPower) ? MaxPower : s.MaxPower;
	return temp;
}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	TSet temp(*this);
	temp.InsElem(Elem);
	return temp;
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	TSet temp(*this);
	temp.DelElem(Elem);
	return temp;
}

TSet TSet::operator*(const TSet &s) // пересечение
{
	TSet temp(*this);
	temp.BitField = temp.BitField & s.BitField;//using the method from tbitfield.h
	temp.MaxPower = (MaxPower > s.MaxPower) ? MaxPower : s.MaxPower;
	return temp;
}

TSet TSet::operator~(void) // дополнение
{
	TSet temp(*this);
	temp.BitField = ~temp.BitField;//using the method from tbitfield.h
	return temp;
}

// перегрузка ввода/вывода

istream &operator>>(istream &istr, TSet &s) // ввод
{
	int temp;
	char ch;
	do 
	{
		istr >> ch;
	}
	while (ch != '{');
	do
	{
		istr >> temp;
		s.InsElem(temp);
		do 
		{ 
			istr >> ch; 
		} 
		while ((ch != ',') && (ch != '}'));
	}
	while (ch != '}');
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	int i = 0;
	bool check = 0;
	ostr << "{";
	while (i < s.MaxPower)
	{
		if (s.IsMember(i) != 0)
		{
			if(check!=0)// to not display the excess ','
			{
				ostr << ",";
			}
			ostr << i;
			check = 1;
		}
		i++;
	}
	ostr << "}";
	return ostr;
}

#include "tbitfield.h"
TBitField::TBitField(int len){
	MemLen = len / sizeof(TELEM) * 8 + 1;
	if (len > 0)
		BitLen = len;
	else
		throw ("Size of BitField incorrect");
	pMem = new TELEM[MemLen];
	if (pMem!=NULL){
		for(int i = 0; i<MemLen; i++)
			pMem[i]=0;
	}
}

TBitField::TBitField(const TBitField &bf){
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	if (pMem != NULL)
		for (int i=0; i<MemLen;i++)
			pMem[i] = bf.pMem[i];
}

TBitField::~TBitField(){
	delete pMem;
	pMem = NULL;
}

int TBitField::GetMemIndex(const int n) const{
	int a = sizeof(TELEM) * 8;
	
	if (n % a == 0)
		return n / a;
	else
		return n / a + 1;
}

TELEM TBitField::GetMemMask(const int n) const{
	int a = sizeof(TELEM) * 8;// bit in one element of array
	int numOfElement = n / a;// num of element in array with current bit
	int numbit = n%a;//Number bit in current element
	
	int mask = 1;
	mask = (mask << numbit)&pMem[numOfElement];
	return mask;
}

int TBitField::GetLength(void) const{
	return BitLen;
}

void TBitField::SetBit(const int n){
	if ((n >= 0) && (n<BitLen))
		pMem[GetMemIndex(n)]|= GetMemMask(n);
	else
		throw ("bit(n) not valid");
}

void TBitField::ClrBit(const int n){
	if ((n>=0) && (n< BitLen))
		pMem[GetMemIndex(n)] &= ~GetMemMask(n);
	else
		throw ("bit(n) not valid");
}

int TBitField::GetBit(const int n) const{
	if ((n>=0) && (n< BitLen))
		return pMem[GetMemIndex(n)] & GetMemMask(n);
	else
		throw ("bit(n) not valid");
}

TBitField& TBitField::operator=(const TBitField &bf){
	BitLen = bf.BitLen;
	if (MemLen != bf.MemLen){
		MemLen = bf.MemLen;
		if (pMem != NULL )
			delete pMem;
		pMem = new TELEM[MemLen];
	}
	else
		throw ("Entered length of bitfield != lentgh this bitfield ");
	if (pMem != NULL)
		for (int i = 0; i<MemLen;i++)
			pMem[i] = bf.pMem[i];
	else
		throw ("Bitfield is empty");
	return *this;
}






int TBitField::operator==(const TBitField &bf) const{
	unsigned res = 1;
	if (BitLen != bf.BitLen)
		res = 0;
	else
		for (int i = 0; i<MemLen; i++)
			if (pMem[i] != bf.pMem[i] ){
				res = 0;
				break;
			}
	return res;
}

int TBitField::operator!=(const TBitField &bf) const{
	unsigned res = 0;
	if (BitLen != bf.BitLen)
		res = 1;
	else
		for (int i = 0; i<MemLen; i++)
			if (pMem[i] != bf.pMem[i] ){
				res = 1;
				break;
			}
	return res;
}

TBitField TBitField::operator|(const TBitField &bf){
	int len = BitLen;
	if ( bf.BitLen > len)
		len =bf.BitLen;
	TBitField buf(len);
	for (int i=0; i<MemLen; i++)
		buf.pMem[i] = pMem[i];
	for (int i=0; i<bf.MemLen; i++)
		buf.pMem[i] |= bf.pMem[i];
	return buf;
}

TBitField TBitField::operator&(const TBitField &bf){
	int len = BitLen;
	if ( bf.BitLen > len)
		len =bf.BitLen;
	TBitField buf(len);
	for (int i=0; i<MemLen; i++)
		buf.pMem[i] = pMem[i];
	for (int i=0; i<bf.MemLen; i++)
		buf.pMem[i] &= bf.pMem[i];
	return buf;
}
TBitField TBitField::operator~(void){ //not
	int len = BitLen;
	TBitField buf(len);
	for (int i=0; i<MemLen; i++)
		buf.pMem[i]=~pMem[i];
	return buf;
}
istream &operator>>(istream &istr, TBitField &bf){
	int i=0;
	char ch;
	do{
		istr >> ch;
	} while (ch!= ' ');
	while(1){
		istr >> ch;
		if ( ch == '0')
			bf.ClrBit(i++);
		else
			if (ch == '1')
				bf.SetBit(i++);
			else{
				throw("Incorrect input");
				break;
			}
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf){
	for (int i = bf.BitLen - 1; i >= 0; i--){
		ostr << bf.GetBit(i) << ' ';
	}
	ostr << endl;
	return ostr;
}

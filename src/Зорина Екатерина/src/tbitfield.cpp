// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#include "tbitfield.h"


TBitField::TBitField(int len)
{
	if (len <= 0)
		throw "Incorrect length of bitfield";
	BitLen = len;
	MemLen = (BitLen + 31) >> 5;
	//31 = длина TELEM в битах - 1
	// >> 5 равноисльно делению на 32, т. е. 2^5 
	//добавляем 31 для нелишения битов из конца поля 
	pMem = new TELEM [MemLen];
	if (pMem == NULL)
		throw "Bitfield is not exists";
	for (int i = 0; i < MemLen; i++)
		pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM [MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = bf.pMem[i];
}

TBitField::~TBitField() // деструктор
{
	delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if (n < 0 || n >= BitLen)
		throw "Incorrect index";
	return n >> 5;
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if (n < 0 || n >= BitLen)
		throw "Incorrect index";
	return (1 << (n % 31)); 
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
  return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if (n < 0 || n >= BitLen)
		throw "Incorrect index";
	TELEM temp = GetMemMask(n);
	pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] | temp;
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if (n < 0 || n >= BitLen)
		throw "Incorrect index";
	TELEM temp = GetMemMask(n);
	temp = ~ temp;
	pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] & temp;
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	if (n < 0 || n >= BitLen)
		throw "Incorrect index";
  return pMem[GetMemIndex(n)] & GetMemMask(n);
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{	
	if (bf == *this)
		return *this;
	//защита от самоприсваивания
	if (pMem == NULL)
		throw "Bitfield is not exists";
	delete[] pMem;
	BitLen = bf.GetLength();
	MemLen = (BitLen + 31) >> 5;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = bf.pMem[i];
	return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	if (GetLength() != bf.GetLength())
		return 0;
	for (int i = 0; i < MemLen; i++)
		if (pMem[i] != bf.pMem[i])
			return 0;
	return 1;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
	if (GetLength() != bf.GetLength())
		return 1; 
	for (int i = 0; i < MemLen; i++)
		if (pMem[i] != bf.pMem[i])
			return 1;
	return 0;
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	TBitField Temp(max(BitLen, bf.BitLen));
	for (int i = 0; i < max(MemLen, bf.MemLen); i++)
		Temp.pMem[i] = pMem[i] | bf.pMem[i];
	return Temp;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	TBitField Temp(max(BitLen, bf.BitLen));
	for (int i = 0; i < min(BitLen, bf.BitLen); i++)
		Temp.pMem[i] = pMem[i] & bf.pMem[i];
	return Temp;
}

TBitField TBitField::operator~(void) // отрицание
{
	TBitField Temp(BitLen);
	for (int i = 0; i < BitLen; i++)
	{
		if (GetBit(i) == 0) 
			Temp.SetBit(i);
		else
			Temp.ClrBit(i);
	}
	return Temp;
}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	char b;
	int i = 0;
	while (true)
	{
		istr >> b;
		if (b == '0')
			bf.ClrBit(i);
		else if (b == '1')
			bf.SetBit(i);
		else 
			break;
		i++;
	}
	bf.BitLen = i;
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for (int i = 0; i < bf.BitLen; i++)
		if (bf.GetBit(i))
			ostr << '1';
		else
			ostr << '0';
	return ostr;
}

// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tset.cpp - Copyright (c) Гергель В.П. 04.10.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Множество - реализация через битовые поля
#include <iostream>
using namespace std;
#include "tset.h"

TSet::TSet(int mp) : BitField(mp)
{
	MaxPower = mp;
}

// конструктор копирования
TSet::TSet(const TSet &s) : BitField(s.BitField)
{
	MaxPower = s.MaxPower;
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf)
{
	MaxPower = bf.GetLength();
}

TSet::operator TBitField()
{
	return TBitField(MaxPower);
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
	if (BitField.GetBit(Elem))
		return 1;
	return 0;
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	if (Elem < 0 || Elem > MaxPower)
		throw "Incorrect element";
	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	if (Elem < 0 || Elem > MaxPower)
		throw "Incorrect element";
	BitField.ClrBit(Elem);
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{
	MaxPower = s.MaxPower;
	BitField = s.BitField;
	return *this;
}

int TSet::operator==(const TSet &s) const // сравнение
{
	return ((MaxPower == s.MaxPower) & (BitField == s. BitField));
}

int TSet::operator!=(const TSet &s) const // сравнение
{
	return !((MaxPower == s.MaxPower) & (BitField == s. BitField));
}

TSet TSet::operator+(const TSet &s) // объединение
{
	TSet Temp = (BitField | s.BitField);
	return Temp;
}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	if (Elem < 0 || Elem > MaxPower)
		throw "Incorrect element";
	TSet result = *this;
	result.BitField.SetBit(Elem);
	return result;
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	if (Elem < 0 || Elem > MaxPower)
		throw "Incorrect element";
	BitField.ClrBit(Elem);
	return *this;
}

TSet TSet::operator*(const TSet &s) // пересечение
{
	TSet Temp(max(s.MaxPower, MaxPower));
	Temp.BitField = BitField & s.BitField;
	return Temp;
}

TSet TSet::operator~(void) // дополнение
{
	return (~BitField);
}

// перегрузка ввода/вывода

istream &operator>>(istream &istr, TSet &s) // ввод
{
	//формат ввода: { A1 A2 A3 ... An }
	int temp = 0;
	char b;
	while (b != '}')
	{
		istr >> temp;
		s.InsElem(temp);
	}
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	//формат вывода: { A1 A2 A3 ... An }
	ostr << "{ ";
	for(int i = 0; i < s.MaxPower; i++)
		if (s.IsMember(i))
			ostr << i << ' ';
	ostr << '}';
	return ostr;
}
